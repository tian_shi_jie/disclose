## docker
 
拉取镜像
+ docker pull nginx 

下载的镜像在本地

查看看镜像
+ docker images 

删除解析
docker rmi 解析名称 偶然镜像id 

容器启动 

docker --help 

docker run 设置项 镜像名 
--name  名称
-d  后台运行 
--restart=always 开机自启动 
-p   端口映射


docker ps 
docker ps -a 

docker rm 容器id 容器名称

docker restart 
docker start 容器id 容器名称
docker stop 容器id 容器名称
 
docker update  设置项  



进入容器 和修改内容

docker exec it 容器id  /bin/bash 
docker commit -m '' 容器id   新的容器id

保存镜像

docker save -o xx.tar 镜像名称 

加载镜像 
docker load -i xxx.tar 


