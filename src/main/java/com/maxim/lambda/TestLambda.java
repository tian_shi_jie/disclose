package com.maxim.lambda;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class TestLambda {

    public static void main(String[] args) {

        Consumer<String> cp = rt -> {
            System.out.println("-----------------------------");
            System.out.println("rt ::: " + rt);
            System.out.println("-----------------------------");
        };

        cp.accept("145421");

        List<Integer> list  = new ArrayList<>();

        for(int i= 0 ;i<=100;i++){
            list.add(i);
        }

        for(Integer i : list){

        }

        System.out.println("======================================");
        Stream st=Arrays.asList(1,2,3,4,5).stream().filter(x->{
            System.out.print(x);
            return  x>3;
        });

        Arrays.asList(1,2,3,4,5).forEach(t -> {
            System.out.println(t + 1);
        });

        System.out.println("======================================");

        Map<String, Integer> items = new HashMap<>();

        items.put("A", 10);
        items.put("B", 20);
        items.put("C", 30);
        items.put("D", 40);
        items.put("E", 50);
        items.put("F", 60);

        items.forEach((k,v) -> {
            System.out.println("ddddddddfdfd");

            System.out.println("sssssssssssssssss");
        });

        items.forEach((k,v)->System.out.println("Item : " + k + " Count : " + v));


        items.forEach((k,v)->{
            System.out.println("Item : " + k + " Count : " + v);
            if("E".equals(k)){
                System.out.println("Hello E");
            }
        });



    }
}
