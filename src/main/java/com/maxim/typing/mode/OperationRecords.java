package com.maxim.typing.mode;

public class OperationRecords {


    private String outputLetter ;

    private String inputLetter ;

    private boolean isCorrect;

    public OperationRecords(String outputLetter, String inputLetter) {
        this.outputLetter = outputLetter;
        this.inputLetter = inputLetter;
        if(outputLetter ==null && inputLetter == null){
            this.isCorrect = true;
        }else if(outputLetter != null && inputLetter != null){
            this.isCorrect = outputLetter.equals(inputLetter);
        }else {
            this.isCorrect = false;
        }
    }
}
