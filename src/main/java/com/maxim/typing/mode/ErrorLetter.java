package com.maxim.typing.mode;

import com.oracle.webservices.internal.api.databinding.DatabindingMode;

import java.util.List;


public class ErrorLetter {

    private String letter;
    private int count;
    private List<String> errLetters;

    public ErrorLetter(String letter, int count, List<String> errLetters) {
        this.letter = letter;
        this.count = count;
        this.errLetters = errLetters;
    }

    public ErrorLetter() {
    }

    public String getLetter() {
        return letter;
    }

    public int getCount() {
        return count;
    }

    public List<String> getErrLetters() {
        return errLetters;
    }

    public void setLetter(String letter) {
        this.letter = letter;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setErrLetters(List<String> errLetters) {
        this.errLetters = errLetters;
    }

    @Override
    public String toString() {
        return "{" +
                "letter='" + letter + '\'' +
                ", count=" + count +
                ", errLetters=" + errLetters +
                '}';
    }

}
